﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace UDPSensorReceiver
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.Title = "Server Receiver";

            //Creates an IPEndPoint to record the IP Address and port number of the sender.  
            //IPAddress ip = IPAddress.Any;
            IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 11111);
            //IPEndPoint object will allow us to read datagrams sent from another source.

            //Creates a UdpClient for reading incoming data.
            UdpClient udpServer = new UdpClient(RemoteIpEndPoint);

            try
            {
                // Blocks until a message is received on this socket from a remote host (a client).
                Console.WriteLine("Server is blocked");

                while (true)
                {
                    Byte[] receiveBytes = udpServer.Receive(ref RemoteIpEndPoint);
                    //Server is now activated");

                    string receivedData = Encoding.ASCII.GetString(receiveBytes);

                    Console.WriteLine(receivedData);

                    Console.WriteLine("This message was sent from " +
                                      RemoteIpEndPoint.Address.ToString() +
                                      " on their port number " +
                                      RemoteIpEndPoint.Port.ToString());
                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
