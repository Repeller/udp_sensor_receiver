﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ClientSensor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client Sender board";

            // CO in mg
            double Co;

            // NOx in ug
            double NOx;

            // small particles concentration(Alarm, High, Normal, Low)
            string concentration;

            UdpClient udpClient = new UdpClient();
            udpClient.EnableBroadcast = true;

            IPAddress ip = IPAddress.Broadcast;

            int number = 0;

            IPEndPoint RemoteIpEndPoint = new IPEndPoint(ip, 9999);

            #region other code

            //int number = 0;

            //IPAddress ip = IPAddress.Parse("192.168.103.233"); //
            //UdpClient udpClient = new UdpClient("192.168.103.233", 7000);

            //IPEndPoint RemoteIpEndPoint = new IPEndPoint(ip, 7000); //
            ////udpClient.Connect(RemoteIpEndPoint); //

            //Console.Write("State name: ");
            //String name = Console.ReadLine();

            //for (int i = 0; i < 5000; i++)
            //{
            //    Byte[] sendBytes = Encoding.ASCII.GetBytes(name + " " + number + " hello");

            //    udpClient.Send(sendBytes, sendBytes.Length); //, (RemoteEndPoint NOT in 1-1 communication);
            //    Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
            //    //Client is now activated");

            //    string receivedData = Encoding.ASCII.GetString(receiveBytes);
            //    Console.WriteLine(receivedData);
            //    number++;

            //    Thread.Sleep(100);
            //}

            #endregion

            while (true)
            {
                Console.WriteLine(number);
                Byte[] sendBytes = Encoding.ASCII.GetBytes("The number is:" + number);

                udpClient.Send(sendBytes, sendBytes.Length, RemoteIpEndPoint); //, (RemoteEndPoint NOT in 1-1 communication);

                number++;

                Thread.Sleep(1000);
            }
        }
    }
}
